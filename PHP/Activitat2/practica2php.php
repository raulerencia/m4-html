<!DOCTYPE html>
 <html lang="ca">
  <head>
   <meta charset="utf-8">
   <link rel="stylesheet" href="./practica2php.css">
   <title>Activitat Fitxers</title>
  </head>
  <body>
    <header>Activitat Fitxers</header>
    <div class="cos">
      <?php
	     function obtFecha(){
	      $data = date('d-m-Y H:i:s');
	      return $data;
      }
	     $fichero = file($_FILES['fitxer']['tmp_name']);
	     $contador = -1;
	     foreach ($fichero as $nlinia => $linia){
	        $contador=$contador+1;
	     }
	     $write=fopen('fitxers/fitxerResultant','w');
	     foreach ($fichero as $nlinia => $linia){
	        if($nlinia==$contador){
	           fwrite($write, $linia."\n"."Text pujat el: ".obtFecha());
	        }else{
	           fwrite($write, $linia);
	        }
	     }
       fclose($write);
	     echo "<br>";
	     if($_FILES['fitxer']['error']>0){
	        echo "Hi ha hagut errors durant la pujada";
	     }else{
	        echo "Arxiu pujat correctament";
	     }
	     echo "<br><br>";
	     echo "Contingut de l'arxiu:";
	     $upFile=file('fitxers/fitxerResultant');
	     echo "<br>";
	     foreach ($upFile as $nlinia2 => $linia2){
	        echo $linia2."<br>";
	     }
	     echo "<br><br>";
      ?>
    </div>
    <footer>Raúl Erencia</footer>
  </body>
</html>
